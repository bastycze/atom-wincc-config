## Prerequisites

Install atom-shell-commands:
```bash
apm install atom-shell-commands
```

## Syntax check command

Copy "atom-shell-commands" key and its content from atom-wincc-config/config.cson into your config.cson file.
Replace PROJECT_NAME with your project name and PATH_TO_WINCC with your WinCC installation path.

Default keybinding is Ctrl + Alt + s.

## Key bindings

Paste the content from atom-wincc-config/packages/atom-shell-commands/keymaps/atom-shell-commands.cson into .atom/packages/atom-shell-commands/keymaps/atom-shell-commands.cson 
By default, F5 will jump to previous error will F6 to the next one, you can change it in atom-shell-commands.cson.

